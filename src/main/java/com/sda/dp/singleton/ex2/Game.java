package com.sda.dp.singleton.ex2;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class Game {
    private Player player;
    private int rounds;
    private int number1;
    private int number2;
    private char operation;
    private int result;
    private int answer;
    private int points;

    public Game(Player player, int rounds) {
        this.player = player;
        this.rounds = rounds;
        this.points = 0;
    }

    public void nextMathOperation(){
        Random random = new Random();
        number1 = random.nextInt(MySetting.getInstance().getNumber1Range());
        number2 = random.nextInt(MySetting.getInstance().getNumber2Range());
        operation = MySetting.getInstance().getAlvilibleOperations().charAt(random.nextInt(MySetting.getInstance().getAlvilibleOperations().length()));

        System.out.println("Liczba 1 wynosi: "+number1);
        System.out.println("Liczba 2 wynosi: "+number2);
        System.out.println("Dzialanie wylosowane: "+operation);
        System.out.println("Pelne dzialanie: "+number1+""+operation+""+number2+"=");
        switch (operation){
            case '+' : result = sum();
                break;
            case '-': result =min();
                break;
            case '*': result =mult();
                break;
            case '/': result = divi();
                break;
            default:
                System.out.println("wrong math operation");
                break;
        }
    }

    public void checkAnswer(){
        System.out.println("Podaj wynik");
       Scanner scanner = new Scanner(System.in);
        try {
            answer = scanner.nextInt();
        }catch (InputMismatchException ime){
            System.out.println("Podaj liczbe");
        }
        if(answer==result) {
            System.out.println("Dobrze!");
            points++;
        }
        else System.out.println("Zle prawidłowy wynik to:" + result);
        if(rounds>0)System.out.println("pozostało rund: "+(--rounds));
        else System.out.println("Koniec gry! Toje punkty: "+ points);
    }

    private int divi() {
        return number1/number2;
    }

    private int mult() {
        return number1*number2;
    }

    private int min() {
        return number1-number2;
    }

    private int sum() {
        return number1+number2;
    }

    public int getRounds() {
        return rounds;
    }
}
