package com.sda.dp.singleton.ex2;

public class Main {
    public static void main(String[] args) {
        Game game = new Game(new Player(),MySetting.getInstance().getRounds());
        while (game.getRounds()>0) {
            game.nextMathOperation();
            game.checkAnswer();
        }
    }
}
