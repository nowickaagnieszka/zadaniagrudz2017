package com.sda.dp.singleton.ex2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileReader {
    private Scanner scanner;

    public FileReader(){
            }
    public void readFile(){
        try{
            scanner = new Scanner(new File("config.txt"));
            scanner.nextLine();

        }catch (FileNotFoundException e){
            e.printStackTrace();
        }
    }
}
