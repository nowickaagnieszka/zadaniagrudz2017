package com.sda.dp.singleton.ex2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MySetting {
    private static MySetting ourInstance = new MySetting();
    private int number1Range;
    private int number2Range;
    private String alvilibleOperations;
    private int rounds;

    public static MySetting getInstance() {
        return ourInstance;
    }

    private MySetting() {
        File file = new File("config.txt");
        try {
           Scanner scanner = new Scanner(file);
            number1Range = Integer.parseInt(scanner.nextLine());
            number2Range = Integer.parseInt(scanner.nextLine());
            alvilibleOperations = scanner.nextLine();
            rounds = Integer.parseInt(scanner.nextLine());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public int getNumber1Range() {
        return number1Range;
    }

    public int getNumber2Range() {
        return number2Range;
    }

    public String getAlvilibleOperations() {
        return alvilibleOperations;
    }

    public int getRounds() {
        return rounds;
    }
}
