package com.sda.dp.eventDispatcher.Android;

public class Notyfication {
    private String jakiePowiadomienie;

    public Notyfication(String jakiePowiadomienie) {
        this.jakiePowiadomienie = jakiePowiadomienie;
    }

    public String getJakiePowiadomienie() {
        return jakiePowiadomienie;
    }

    public void setJakiePowiadomienie(String jakiePowiadomienie) {
        this.jakiePowiadomienie = jakiePowiadomienie;
    }
}
