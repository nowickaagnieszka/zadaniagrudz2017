package com.sda.dp.treadpool.zad1KalkulatorSilni;

public class Request implements Runnable{
    private long number;
    AsynchronousFactorialCalculator ascC;

    public Request(long number, AsynchronousFactorialCalculator ascC) {
        this.number = number;
        this.ascC = ascC;
    }

    public long silnia(long i){
        try{ Thread.sleep(100);
        }catch (InterruptedException ie ){
            ie.printStackTrace();
        }
            if(i<1){
            return 1;
            } else {
                return i* silnia(i-1);
            }
        }

    @Override
    public void run() {
        long silnia =silnia(number);
        ascC.addToList(number+"!=" +silnia);
        ascC.showList();
    }
}
