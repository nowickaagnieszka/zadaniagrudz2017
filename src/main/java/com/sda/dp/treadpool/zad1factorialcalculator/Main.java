package com.sda.dp.treadpool.zad1KalkulatorSilni;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        AsynchronousFactorialCalculator afc = new AsynchronousFactorialCalculator();
        Scanner sc = new Scanner(System.in);
        int number;
        while(true){
           number = sc.nextInt();
            afc.obliczSilnia(number);
        }
    }
}
