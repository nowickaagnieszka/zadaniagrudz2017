package com.sda.dp.treadpool.zad1KalkulatorSilni;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AsynchronousFactorialCalculator {
    private ExecutorService service = Executors.newFixedThreadPool(3);
    private List<String> lista = new ArrayList<>();

    public void addToList(String line){
        lista.add(line);
    }
    public void showList(){
        System.out.println(lista);
    }


    public void obliczSilnia(int number) {
        service.submit(new Request(number, this));
    }
}
