package com.sda.dp.observer.SMSStation;

public class Sms {
    private long number;
    private String message;

    public Sms(long number, String message) {
        this.number = number;
        this.message = message;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Sms{" +
                "number=" + number +
                ", message='" + message + '\'' +
                '}';
    }
}
