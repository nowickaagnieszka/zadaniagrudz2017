package com.sda.dp.observer.SMSStation;

import java.util.Observable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SmsStation extends Observable{   //obserwowany, wysyla inf do obserwujacych
    ExecutorService service = Executors.newFixedThreadPool(3);

    public SmsStation(){
        addObserver(new Phone(123456789));
        addObserver(new Phone(987654321));
    }
   public void sendSms(Sms sms){
           service.submit(new Request(sms));

   }
}
