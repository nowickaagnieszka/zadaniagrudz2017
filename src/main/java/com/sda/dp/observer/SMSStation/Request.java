package com.sda.dp.observer.SMSStation;

public class Request implements Runnable {
    private Sms sms;

    public Request(Sms sms) {
        this.sms = sms;
    }

    public void run() {
        try {
            Thread.sleep(1000 * sms.getMessage().length());
            System.out.println("Loading...");
            System.out.println("Wyslano sms: " +sms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
