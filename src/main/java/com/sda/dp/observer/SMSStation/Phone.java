package com.sda.dp.observer.SMSStation;


import java.util.Observable;
import java.util.Observer;

public class Phone implements Observer {
    private long number;

    public Phone(long number) {
        this.number = number;
    }

    @Override
    public void update(Observable observable, Object o) {
        if(o instanceof Sms){
            Sms castedSms = (Sms) o;
            if(this.number == castedSms.getNumber())
                System.out.println(castedSms);
        }

    }
}
