package com.sda.dp.observer.SMSStation;

public class Main {
    public static void main(String[] args) {
        SmsStation smsStation = new SmsStation();
        smsStation.sendSms(new Sms(121123123, "Hello."));
        smsStation.sendSms(new Sms(777555333, "How are U?"));
    }
}
