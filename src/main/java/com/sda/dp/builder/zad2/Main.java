package com.sda.dp.builder.zad2;

import java.util.ArrayList;
import java.util.List;

/*

 */
public class Main {
    public static void main(String[] args) {
        Stamp.Builder builder = new Stamp.Builder();
        List<Stamp> listOfStamps = new ArrayList<>();
        listOfStamps.add(builder.createStamp());
        listOfStamps.add(builder.setFirstMonthNumber(2).setYearNumber1(2).createStamp());
        System.out.println(listOfStamps);
    }
}
