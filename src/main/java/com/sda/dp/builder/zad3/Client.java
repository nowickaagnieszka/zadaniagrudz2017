package com.sda.dp.builder.zad3;

import java.util.ArrayList;
import java.util.List;

public class Client {

    private String name;
    private List<Mail> lista = new ArrayList<>();

    public Client(String name) {
        this.name = name;
    }

    public void readMail (Mail m){
        lista.add(m);
        System.out.println("Klient " + name + " otrzymał maila: " + m.getClass());
    }
}