package com.sda.dp.builder.zad3;

public enum Type {
    UNKNOWN, OFFER, SOCIAL, NOTIFICATIONS, FORUM
}