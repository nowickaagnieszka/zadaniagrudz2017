package com.sda.dp.builder.zad3;

public class Main {

    public static void main(String[] args) {
        MailServer server = new MailServer();
        server.connect(new Client("Ola"));
        server.connect(new Client("Ala"));
        server.connect(new Client("Kasia"));
        server.connect(new Client("Marta"));
        server.connect(new Client("Olek"));

        server.sendMessage(MailFactory.createWarningMail("Error"), new Client("NoReply"));
    }
}