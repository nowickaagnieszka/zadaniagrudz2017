package com.sda.dp.builder.zad1;

public class GameCharacter {
    private String name;
    private int health,mana,numberofpoints;

    public GameCharacter(String name, int health, int mana, int numberofpoints) {
        this.name = name;
        this.health = health;
        this.mana = mana;
        this.numberofpoints = numberofpoints;
    }
    public  static class Builder{

        private String name;
        private int health;
        private int mana;
        private int numberofpoints;

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setHealth(int health) {
            this.health = health;
            return this;
        }

        public Builder setMana(int mana) {
            this.mana = mana;
            return this;
        }

        public Builder setNumberofpoints(int numberofpoints) {
            this.numberofpoints = numberofpoints;
            return this;
        }

        public GameCharacter createGameCharacter() {
            return new GameCharacter(name, health, mana, numberofpoints);
        }
    }
}
