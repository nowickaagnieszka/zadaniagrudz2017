package com.sda.dp.abstractFactory.hotelApp;

import java.time.LocalDateTime;

public class HotelGuest extends Person {
    private String nrKK;

    public HotelGuest(int id, String firtName, String lastName, String email, String nrTel, String ulica, String nrBudynku, String kodPoczt, String miasto, String panstwo, LocalDateTime dataUtwKartoteki, String nrKK) {
        this.id = id;
        this.firtName = firtName;
        this.lastName = lastName;
        this.email = email;
        this.nrTel = nrTel;
        this.ulica = ulica;
        this.nrBudynku = nrBudynku;
        this.kodPoczt = kodPoczt;
        this.miasto = miasto;
        this.panstwo = panstwo;
        this.dataUtwKartoteki = dataUtwKartoteki;
        this.nrKK = nrKK;
    }

}
