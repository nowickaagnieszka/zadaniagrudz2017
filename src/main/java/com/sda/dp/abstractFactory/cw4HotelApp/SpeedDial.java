package com.sda.dp.abstractFactory.hotelApp;

public class SpeedDial {
    enum SpeedDialButtom {B1, B2, B3, B4, B5, B6, B7, B8, B9, B0};
    private SpeedDialButtom buttom;
    private String internalNr;

    public SpeedDial(SpeedDialButtom buttom, String internalNr) {
        this.buttom = buttom;
        this.internalNr = internalNr;
    }
}
