package com.sda.dp.abstractFactory.hotelApp;

public class HotelRoom {
    enum RoomType {LOW, STANDARD, HIGHT};
    private int id;
    private RoomType roomType;

    public HotelRoom(int id, RoomType roomType) {
        this.id = id;
        this.roomType = roomType;
    }
}
