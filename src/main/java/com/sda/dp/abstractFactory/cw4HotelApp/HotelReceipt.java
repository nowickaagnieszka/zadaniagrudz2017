package com.sda.dp.abstractFactory.hotelApp;

import java.time.LocalDateTime;

public class HotelReceipt {
    private int idReceipt;
    private LocalDateTime dataRejstr;
    private LocalDateTime dataWyrejstr;
    private HotelGuest guest;
    private HotelRoom room;

    public HotelReceipt(int idReceipt, LocalDateTime dataRejstr, LocalDateTime dataWyrejstr, HotelGuest guest, HotelRoom room) {
        this.idReceipt = idReceipt;
        this.dataRejstr = dataRejstr;
        this.dataWyrejstr = dataWyrejstr;
        this.guest = guest;
        this.room = room;
    }
}
