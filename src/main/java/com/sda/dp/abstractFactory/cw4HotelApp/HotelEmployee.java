package com.sda.dp.abstractFactory.hotelApp;

import java.time.LocalDateTime;

public class HotelEmployee extends Person {
    enum position {RECEPCJONISTA, POKOJOWKA, KELNER, KIEROWNIK};
    private String nrKontaDoWypłaty;
    private position stanowisko;
    private double wynagrodzenie;
    private boolean statusEmployeee;

    public HotelEmployee(int id, String firtName, String lastName, String email, String nrTel, String ulica,
                         String nrBudynku, String kodPoczt, String miasto, String panstwo, LocalDateTime dataUtwKartoteki,
                         String nrKontaDoWypłaty, double wynagrodzenie, position stanowisko) {
        this.id = id;
        this.firtName = firtName;
        this.lastName = lastName;
        this.email = email;
        this.nrTel = nrTel;
        this.ulica = ulica;
        this.nrBudynku = nrBudynku;
        this.kodPoczt = kodPoczt;
        this.miasto = miasto;
        this.panstwo = panstwo;
        this.dataUtwKartoteki = dataUtwKartoteki;
        this.nrKontaDoWypłaty = nrKontaDoWypłaty;
        this.wynagrodzenie = wynagrodzenie;
        this.stanowisko = stanowisko;
        this.statusEmployeee = true;
    }
}
