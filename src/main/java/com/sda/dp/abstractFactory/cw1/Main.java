package com.sda.dp.abstractFactory.cw1;

public class Main {
    public static void main(String[] args) {
        AbstractPC pc = SamsungPC.createSamsungPC();
        System.out.println(pc);
    }
}
