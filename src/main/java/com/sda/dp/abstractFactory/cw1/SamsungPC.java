package com.sda.dp.abstractFactory.cw1;

public class SamsungPC extends AbstractPC {
    public SamsungPC(String computerName, COMPUTER_BRAND brand, int cpu_power, double gpu_power, boolean isOverclocked) {
        super(computerName, brand, cpu_power, gpu_power, isOverclocked);

        System.out.println("Tworzymy kompa: " + this.getComputerName());
    }
    public static AbstractPC createSamsungPC(){
        return new SamsungPC("Samsung", COMPUTER_BRAND.SAMSUNG, 130, 100, true);
    }
}
