package com.sda.dp.abstractFactory.cw1;

public abstract class AbstractPC {
    private String computerName;
    private COMPUTER_BRAND brand;
    private int cpu_power;
    private double gpu_power;
    private boolean isOverclocked;

    @Override
    public String toString() {
        return "AbstractPC{" +
                "computerName='" + computerName + '\'' +
                ", brand=" + brand +
                ", cpu_power=" + cpu_power +
                ", gpu_power=" + gpu_power +
                ", isOverclocked=" + isOverclocked +
                '}';
    }

    public AbstractPC(String computerName, COMPUTER_BRAND brand, int cpu_power, double gpu_power, boolean isOverclocked) {
        this.computerName = computerName;
        this.brand = brand;
        this.cpu_power = cpu_power;
        this.gpu_power = gpu_power;
        this.isOverclocked = isOverclocked;
    }

    public String getComputerName() {
        return computerName;
    }

    public void setComputerName(String computerName) {
        this.computerName = computerName;
    }

    public COMPUTER_BRAND getBrand() {
        return brand;
    }

    public void setBrand(COMPUTER_BRAND brand) {
        this.brand = brand;
    }

    public int getCpu_power() {
        return cpu_power;
    }

    public void setCpu_power(int cpu_power) {
        this.cpu_power = cpu_power;
    }

    public double getGpu_power() {
        return gpu_power;
    }

    public void setGpu_power(double gpu_power) {
        this.gpu_power = gpu_power;
    }

    public boolean isOverclocked() {
        return isOverclocked;
    }

    public void setOverclocked(boolean overclocked) {
        isOverclocked = overclocked;
    }
}
